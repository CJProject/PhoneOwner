//
//  PhoneOwnerApp.swift
//  PhoneOwner
//
//  Created by Alexey Sigay on 06.02.2021.
//

import SwiftUI

@main
struct PhoneOwnerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
